package com.dobda.apiserver.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

import java.io.File;
import java.nio.file.Paths;

@Configuration
public class WebConfig implements WebMvcConfigurer{
	
	@Autowired
	private DobdaProperties dobdaProperties;
	
	@Override
	public void addResourceHandlers(ResourceHandlerRegistry registry) {
		File uploadDir = new File(dobdaProperties.getUploadPath());
		String uploadPath = uploadDir.getAbsolutePath();
        String[] subPaths = {"public", "auth", "user", "helper", "admin"};
        for (String subPath: subPaths) {
            File subDir = Paths.get(uploadPath, subPath).toFile();
            if(!subDir.exists()) {
                subDir.mkdirs();
            }
            registry.addResourceHandler("/" + subPath + "/upload/**")
                    .addResourceLocations("file:///" + subDir.getAbsolutePath() + File.separator);
        }
	}
	
}
