package com.dobda.apiserver.common.util.openbank.payment;

import lombok.Builder;
import lombok.Data;

import java.util.Arrays;
import java.util.List;

@Data
public class DepositRequest {

	private String cntr_account_type = "N"; // 기관 약정 계좌(고정값 "N")
	private String cntr_account_num; // // 기관 약정 계좌
	private String wd_pass_phrase; //입금이체용 암호(테스트 시 NONE)
	private String wd_print_content; // 출금 계좌 출력 내역
	private String name_check_option = "on"; // 수취인 검증 여부
	private String tran_dtime; // 거래일시
	private String req_cnt = "1"; // 입금요청건수 (고정값 "1")
	private List<Details> req_list;
	
	@Builder
    public DepositRequest(
            String cntr_account_num, String wd_pass_phrase, String wd_print_content,
            String tran_dtime, Details req_list) {
        this.cntr_account_num = cntr_account_num;
        this.wd_pass_phrase = wd_pass_phrase;
        this.wd_print_content = wd_print_content;
        this.tran_dtime = tran_dtime;
        this.req_list = Arrays.asList(req_list);
    }

    @Data
	public static class Details{
		private String tran_no = "1"; // 거래순번 (고정값 "1")
		private String bank_tran_id; // 기관(DOBDA)에서 부여한 거래 id
		private String fintech_use_num; // 입금 계좌 핀테크 번호
		private String print_content; // 입금 계좌 출력 내역
		private Long tran_amt; // 거래금액
		private String req_client_name; // 의뢰 고객 이름
		private String req_client_fintech_use_num; // 의뢰 고객 핀테크 번호
		private String req_client_num; // 의뢰 고객 id
		private String transfer_purpose = "TR"; // 이체 용도(고유값 "TR")

		@Builder
        public Details(
                String bank_tran_id, String fintech_use_num, String print_content,
                Long tran_amt, String req_client_name,
                String req_client_fintech_use_num, String req_client_num) {
            this.bank_tran_id = bank_tran_id;
            this.fintech_use_num = fintech_use_num;
            this.print_content = print_content;
            this.tran_amt = tran_amt;
            this.req_client_name = req_client_name;
            this.req_client_fintech_use_num = req_client_fintech_use_num;
            this.req_client_num = req_client_num;
        }
    }
}
