package com.dobda.apiserver.common.enums;

import javax.persistence.AttributeConverter;
import javax.persistence.Converter;

@Converter(autoApply = true)
public class SmsAuthKeyStateConverter implements AttributeConverter<SmsAuthKeyState, Integer> {
    @Override
    public Integer convertToDatabaseColumn(SmsAuthKeyState memberType) {
        if (memberType == null) {
            return null;
        }
        return memberType.getCode();
    }

    @Override
    public SmsAuthKeyState convertToEntityAttribute(Integer integer) {
        if(integer == null) {
            return null;
        }
        return SmsAuthKeyState.fromCode(integer);
    }
}
