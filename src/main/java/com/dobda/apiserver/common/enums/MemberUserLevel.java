package com.dobda.apiserver.common.enums;

import com.fasterxml.jackson.annotation.JsonValue;
import lombok.Getter;

import java.util.Arrays;
import java.util.Map;
import java.util.stream.Collectors;

public enum MemberUserLevel {
    USER(10),
    HELPER(40);

    private static final Map<Integer, MemberUserLevel> intToEnum =
            Arrays.stream(values()).collect(Collectors.toMap(MemberUserLevel::getCode, e -> e));

    @Getter
    @JsonValue
    private final Integer code;

    private MemberUserLevel(int code)
    {
        this.code = code;
    }

    public static MemberUserLevel fromCode(int code) {
        MemberUserLevel result = intToEnum.get(code);
        return result;
    }
}
