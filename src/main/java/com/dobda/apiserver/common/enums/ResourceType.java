package com.dobda.apiserver.common.enums;

import com.fasterxml.jackson.annotation.JsonValue;
import lombok.Getter;

import java.util.Arrays;
import java.util.Map;
import java.util.stream.Collectors;

public enum ResourceType {
    REQUEST(1),
    REGISTER_HELPER(2),
    ORDER(3),
    REVIEW(4),
    CHAT(5),
    ACCOUNT(6),
    REPORT(7);

    private static final Map<Integer, ResourceType> intToEnum =
            Arrays.stream(values()).collect(Collectors.toMap(ResourceType::getCode, e -> e));

    @Getter
    @JsonValue
    private final Integer code;

    private ResourceType(int code)
    {
        this.code = code;
    }

    public static ResourceType fromCode(int code) {
        ResourceType result = intToEnum.get(code);
        return result;
    }
}