package com.dobda.apiserver.security.config;

import com.dobda.apiserver.common.enums.MemberUserLevel;
import com.dobda.apiserver.config.DobdaProperties;
import com.dobda.apiserver.security.MemberRoles;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.messaging.simp.config.ChannelRegistration;
import org.springframework.messaging.simp.config.MessageBrokerRegistry;
import org.springframework.security.config.annotation.web.messaging.MessageSecurityMetadataSourceRegistry;
import org.springframework.security.config.annotation.web.socket.AbstractSecurityWebSocketMessageBrokerConfigurer;
import org.springframework.web.socket.config.annotation.EnableWebSocketMessageBroker;
import org.springframework.web.socket.config.annotation.StompEndpointRegistry;

import java.util.List;

@Configuration
@EnableWebSocketMessageBroker
public class SocketSecurityConfig extends AbstractSecurityWebSocketMessageBrokerConfigurer {

    @Autowired TopicSubscriptionInterceptor interceptor;

    @Override
    protected void configureInbound(MessageSecurityMetadataSourceRegistry messages) {
        messages
            .simpDestMatchers("/ws/api/user/**").hasRole(MemberRoles.USER.getRole())
            .simpDestMatchers("/ws/api/helper/**").hasAuthority(MemberUserLevel.HELPER.toString())
            .simpDestMatchers("/ws/api/admin/**").hasRole(MemberRoles.ADMIN.getRole())
            .anyMessage().authenticated();
    }

    @Override
    public void configureMessageBroker(MessageBrokerRegistry config) {
        config.setApplicationDestinationPrefixes("/ws/api");
        config.enableSimpleBroker("/ws/topic");
    }

    @Override
    public void registerStompEndpoints(StompEndpointRegistry registry) {
        List<String> origins = DobdaProperties.getInstance().getAllowOrigins();
        registry.addEndpoint("/ws").setAllowedOrigins(origins.toArray(String[]::new)).withSockJS();
    }

    @Override
    protected boolean sameOriginDisabled() {
        return true;
    }

    @Override
    protected void customizeClientInboundChannel(ChannelRegistration registration) {
        registration.interceptors(interceptor);
    }
}
