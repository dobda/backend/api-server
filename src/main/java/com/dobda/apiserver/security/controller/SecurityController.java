package com.dobda.apiserver.security.controller;

import com.dobda.apiserver.api.common.errorhandling.BaseException;
import com.dobda.apiserver.api.common.errorhandling.NotFoundException;
import com.dobda.apiserver.api.member.admin.MemberAdminService;
import com.dobda.apiserver.api.member.user.MemberUserService;
import com.dobda.apiserver.api.member.user.dto.MemberDetailInfo;
import com.dobda.apiserver.common.enums.MemberUserLevel;
import com.dobda.apiserver.security.MemberRoles;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@RestController
public class SecurityController {
    @Autowired @Qualifier("memberAdminService")
    private MemberAdminService memberAdminService;

    @Autowired @Qualifier("memberUserService")
    private MemberUserService memberUserService;

    // 프론트에서 페이지 로드시 항상 myInfo를 호출하니, 여기서 토큰 업데이트를 해주는게 좋을듯 하다.
    @GetMapping(value = "/api/myInfo")
    public ResponseEntity myInfo(Authentication authentication) {
        Long id = (Long) authentication.getPrincipal();
        String role = authentication.getAuthorities().stream()
                .filter(auth -> auth.getAuthority().startsWith("ROLE_"))
                .findAny().get().getAuthority();
        try {
            Object result = null;
            if (role.equals(MemberRoles.ADMIN.getFullRole())) {
                result = memberAdminService.getMemberDetailInfo(id);
            } else if (role.equals(MemberRoles.USER.getFullRole())) {
                MemberDetailInfo memberInfo = memberUserService.getMemberDetailInfo(id);
                if(memberInfo.getIsHelper()) {
                    // 관리자에 의해서 헬퍼로 변경되었는데, 기존에 그냥 사용자로 로그인 되어 있던 경우, 새 토큰 설정
                    if(authentication.getAuthorities().stream()
                            .filter(auth -> auth.getAuthority().equals(MemberUserLevel.HELPER.toString())).findAny().isEmpty()) {
                        // 기존 권한에 헬퍼 권한 추가
                        List<GrantedAuthority> newAuthorities = new ArrayList<>(authentication.getAuthorities());
                        newAuthorities.add(new SimpleGrantedAuthority(MemberUserLevel.HELPER.toString()));
                        SecurityContextHolder.getContext().setAuthentication(
                                new UsernamePasswordAuthenticationToken(
                                        authentication.getPrincipal(),
                                        null,
                                        newAuthorities));
                    }
                } else {
                    // 헬퍼에서 탈퇴했는데, 기존에 헬퍼 상태로 로그인 되어 있던 경우, 새 토큰 설정
                    if(authentication.getAuthorities().stream()
                            .filter(auth -> auth.getAuthority().equals(MemberUserLevel.HELPER.toString())).count() > 0) {
                        // 기존 권한에서 헬퍼 권한 제거
                        List<GrantedAuthority> newAuthorities = new ArrayList<>(
                                authentication.getAuthorities().stream()
                                        .filter(auth -> !auth.getAuthority().equals(MemberUserLevel.HELPER.toString()))
                                        .collect(Collectors.toList())
                        );
                        SecurityContextHolder.getContext().setAuthentication(
                                new UsernamePasswordAuthenticationToken(
                                        authentication.getPrincipal(),
                                        null,
                                        newAuthorities));
                    }
                }
                result = memberInfo;
            }
            return new ResponseEntity(result, HttpStatus.OK);
        } catch(NotFoundException e) {
            SecurityContextHolder.getContext().setAuthentication(null);
            SecurityContextHolder.clearContext();
            return new ResponseEntity(e.getMessage(), e.getStatusCode());
        } catch (BaseException e) {
            return new ResponseEntity(e.getMessage(), e.getStatusCode());
        }
    }
}
