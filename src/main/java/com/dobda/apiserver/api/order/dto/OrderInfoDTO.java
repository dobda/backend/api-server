package com.dobda.apiserver.api.order.dto;

import com.dobda.apiserver.api.member.user.dto.MemberPublicInfo;
import com.dobda.apiserver.api.order.entity.Order;
import com.dobda.apiserver.api.requestboard.dto.RequestPostListInfoDto;
import com.dobda.apiserver.api.review.dto.ViewReviewDto;
import com.dobda.apiserver.common.enums.OrderState;
import com.dobda.apiserver.common.enums.PaymentMethod;
import lombok.Builder;
import lombok.Data;

import java.math.BigInteger;
import java.time.ZonedDateTime;
import java.util.List;
import java.util.stream.Collectors;

@Data
@Builder
public class OrderInfoDTO {

	private BigInteger id;
    private RequestPostListInfoDto requestInfo;
	private MemberPublicInfo helperInfo;
	private PaymentMethod payMethod;
	private Long amount;
	private ZonedDateTime regTime;
	private OrderState state;
    private List<ViewReviewDto> reviews;
	
	public static OrderInfoDTO fromEntity(Order entity) {
		if(entity == null) {
			return null;
		}
		return builder()
                .id(entity.getId())
                .requestInfo(RequestPostListInfoDto.fromEntity(entity.getRequest()))
				.helperInfo(entity.getMemberHelper() != null
                        ? MemberPublicInfo.fromEntity(entity.getMemberHelper().getMemberUser())
                        : null)
				.payMethod(entity.getPayMethod())
				.amount(entity.getAmount())
				.regTime(entity.getRegTime())
				.state(entity.getState())
                .reviews(entity.getReviewList() != null
                        ? entity.getReviewList().stream().map(ViewReviewDto::fromEntity).collect(Collectors.toList())
                        : null)
				.build();
	}
}
