package com.dobda.apiserver.api.sms.entity;

import com.dobda.apiserver.common.enums.SmsAuthKeyState;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.annotations.DynamicInsert;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.time.ZonedDateTime;

@Entity
@Getter
@Setter
@DynamicInsert
@NoArgsConstructor
public class SmsAuthKey {

    @Id @NotNull @Size(max = 20)
    private String phoneNum;

    @NotNull @Size(min = 6, max = 6)
    private String keyNum;

    @Column(columnDefinition = "DATETIME DEFAULT (now() + INTERVAL 5 MINUTE)", nullable = false)
    private ZonedDateTime expireTime;

    @NotNull
    @Column(columnDefinition = "TINYINT UNSIGNED DEFAULT 0", nullable = false)
    private SmsAuthKeyState state = SmsAuthKeyState.AUTH_WAITING;

    @NotNull
    @Column(columnDefinition = "TINYINT UNSIGNED DEFAULT 1", nullable = false)
    private Integer count = 1;

    @Builder
    public SmsAuthKey(String phoneNum, String keyNum) {
        this.phoneNum = phoneNum;
        this.keyNum = keyNum;
    }
}
