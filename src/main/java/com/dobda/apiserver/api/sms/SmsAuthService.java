package com.dobda.apiserver.api.sms;

import com.dobda.apiserver.api.common.errorhandling.BadRequestException;
import com.dobda.apiserver.api.common.errorhandling.NotFoundException;
import com.dobda.apiserver.api.member.user.MemberUserRepository;
import com.dobda.apiserver.api.sms.entity.SmsAuthKey;
import com.dobda.apiserver.common.enums.SmsAuthKeyState;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.ZonedDateTime;
import java.util.Optional;
import java.util.concurrent.ThreadLocalRandom;

@Service
@Transactional(readOnly = true)
public class SmsAuthService {

    @Autowired private SmsAuthKeyRepository smsAuthKeyRepository;
    @Autowired private MemberUserRepository memberUserRepository;

    @Transactional
    public String sendAuthSms(String receiverNum) {
        receiverNum = receiverNum.replace("-", "");

        if(memberUserRepository.findByPhoneNum(receiverNum) != null) {
            throw new BadRequestException("이미 가입된 번호입니다.");
        }

        String key = createAuthKey();

        Optional<SmsAuthKey> authKeyOption = smsAuthKeyRepository.findById(receiverNum);
        if (!authKeyOption.isEmpty()) {
            SmsAuthKey authKey = authKeyOption.get();
            if (authKey.getCount() > 2 && authKey.getExpireTime().isAfter(ZonedDateTime.now())) {
                throw new BadRequestException("너무 많은 요청이 발생했습니다.\n잠시 후 다시 시도해주세요.");
            }
        }

        // 개발용 주석
//        SmsResponse response = null;
//        try {
//            response = SmsUtils.sendSms(receiverNum, "[DOBDA] 인증번호는 " + key + " 입니다.");
//        } catch (Exception e) {
//            throw new InternalServerErrorException(e);
//        }
//
//        switch (response.getStatusCode()) {
//            case "200":
//            case "202": {
                if(!authKeyOption.isEmpty()) {
                    SmsAuthKey authKey = authKeyOption.get();
                    if (authKey.getExpireTime().isAfter(ZonedDateTime.now())) {
                        authKey.setKeyNum(key);
                        authKey.setCount(authKey.getCount() + 1);
                        authKey.setState(SmsAuthKeyState.AUTH_WAITING);
                        authKey.setExpireTime(ZonedDateTime.now().plusMinutes(5));
                        return key;
                    }
                    smsAuthKeyRepository.deleteById(receiverNum);
                }
                SmsAuthKey newSmsAuthKey = SmsAuthKey.builder()
                        .phoneNum(receiverNum)
                        .keyNum(key)
                        .build();
                smsAuthKeyRepository.save(newSmsAuthKey);

                return key;
//            }
//            case "400": {
//                throw new BadRequestException("잘못된 요청입니다.");
//            }
//            default: {
//                throw new InternalServerErrorException("알 수 없는 이유로 SMS 전송에 실패했습니다.");
//            }
//        }
    }

    // 회원 가입된 후에 인증이 필요한 경우
    @Transactional
    public String sendReAuthSms(String receiverNum) {
        receiverNum = receiverNum.replace("-", "");

        if(memberUserRepository.findByPhoneNum(receiverNum) != null) {
            throw new BadRequestException("이미 가입된 번호입니다.");
        }

        String key = createAuthKey();

        Optional<SmsAuthKey> authKeyOption = smsAuthKeyRepository.findById(receiverNum);
        if (!authKeyOption.isEmpty()) {
            SmsAuthKey authKey = authKeyOption.get();
            if (authKey.getCount() > 2 && authKey.getExpireTime().isAfter(ZonedDateTime.now())) {
                throw new BadRequestException("너무 많은 요청이 발생했습니다.\n잠시 후 다시 시도해주세요.");
            }
        }

        // 개발용 주석
//        SmsResponse response = null;
//        try {
//            response = SmsUtils.sendSms(receiverNum, "[DOBDA] 인증번호는 " + key + " 입니다.");
//        } catch (Exception e) {
//            throw new InternalServerErrorException(e);
//        }
//
//        switch (response.getStatusCode()) {
//            case "200":
//            case "202": {
                if(!authKeyOption.isEmpty()) {
                    SmsAuthKey authKey = authKeyOption.get();
                    if (authKey.getExpireTime().isAfter(ZonedDateTime.now())) {
                        authKey.setKeyNum(key);
                        authKey.setCount(authKey.getCount() + 1);
                        authKey.setState(SmsAuthKeyState.RE_AUTH_WAITING);
                        authKey.setExpireTime(ZonedDateTime.now().plusMinutes(5));
                        return key;
                    }
                    smsAuthKeyRepository.deleteById(receiverNum);
                }
                SmsAuthKey newSmsAuthKey = SmsAuthKey.builder()
                        .phoneNum(receiverNum)
                        .keyNum(key)
                        .build();
                newSmsAuthKey.setState(SmsAuthKeyState.RE_AUTH_WAITING);
                smsAuthKeyRepository.save(newSmsAuthKey);

                return key;
//            }
//            case "400": {
//                throw new BadRequestException("잘못된 요청입니다.");
//            }
//            default: {
//                throw new InternalServerErrorException("알 수 없는 이유로 SMS 전송에 실패했습니다.");
//            }
//        }
    }

    @Transactional
    public void checkAuthSms(String receiverNum, String keyNum) {
        receiverNum = receiverNum.replace("-", "");

        Optional<SmsAuthKey> findAuthKey = smsAuthKeyRepository.findById(receiverNum);
        if(findAuthKey.isEmpty()) {
            throw new NotFoundException("먼저 인증키를 발급 받아주세요.");
        }

        SmsAuthKey authKey = findAuthKey.get();
        if(authKey.getExpireTime().isBefore(ZonedDateTime.now())) {
            throw new BadRequestException("인증키가 만료되었습니다. 새로운 인증키를 발급 받아주세요.");
        }
        if(authKey.getState() != SmsAuthKeyState.AUTH_WAITING) {
            throw new BadRequestException("이미 인증키가 사용되었습니다. 새로운 인증키를 발급 받아주세요.");
        }
        if(!authKey.getKeyNum().equals(keyNum)) {
            throw new BadRequestException("인증키가 일치하지 않습니다.");
        }
        authKey.setState(SmsAuthKeyState.AUTHENTICATED);
        authKey.setExpireTime(ZonedDateTime.now().plusHours(1));
    }

    @Transactional
    public void checkReAuthSms(String receiverNum, String keyNum) {
        receiverNum = receiverNum.replace("-", "");

        Optional<SmsAuthKey> findAuthKey = smsAuthKeyRepository.findById(receiverNum);
        if(findAuthKey.isEmpty()) {
            throw new NotFoundException("먼저 인증키를 발급 받아주세요.");
        }

        SmsAuthKey authKey = findAuthKey.get();
        if(authKey.getExpireTime().isBefore(ZonedDateTime.now())) {
            throw new BadRequestException("인증키가 만료되었습니다. 새로운 인증키를 발급 받아주세요.");
        }
        if(authKey.getState() != SmsAuthKeyState.RE_AUTH_WAITING) {
            throw new BadRequestException("이미 인증키가 사용되었습니다. 새로운 인증키를 발급 받아주세요.");
        }
        if(!authKey.getKeyNum().equals(keyNum)) {
            throw new BadRequestException("인증키가 일치하지 않습니다.");
        }
        authKey.setState(SmsAuthKeyState.RE_AUTHENTICATED);
        authKey.setExpireTime(ZonedDateTime.now().plusHours(1));
    }

    @Scheduled(cron = "0 0 */1 * * *")
    @Transactional
    public void deleteExpiredData() {
        smsAuthKeyRepository.deleteByExpireTimeBefore(ZonedDateTime.now());
    }

    private String createAuthKey() {
        return String.valueOf(ThreadLocalRandom.current().nextInt(100000, 1000000));
    }
}
