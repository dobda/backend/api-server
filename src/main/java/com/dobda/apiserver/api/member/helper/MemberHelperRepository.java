package com.dobda.apiserver.api.member.helper;

import org.springframework.data.jpa.repository.JpaRepository;

import com.dobda.apiserver.api.member.helper.entity.HelperApply;
import com.dobda.apiserver.api.member.helper.entity.MemberHelper;

public interface MemberHelperRepository extends JpaRepository<MemberHelper, Long> {
	
	public HelperApply findByAddress(String address);
	
	
}
