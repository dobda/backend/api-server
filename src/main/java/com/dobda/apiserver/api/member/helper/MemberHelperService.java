package com.dobda.apiserver.api.member.helper;


import com.dobda.apiserver.api.common.errorhandling.BadRequestException;
import com.dobda.apiserver.api.common.errorhandling.InternalServerErrorException;
import com.dobda.apiserver.api.common.errorhandling.NotFoundException;
import com.dobda.apiserver.api.common.errorhandling.UnauthorizedException;
import com.dobda.apiserver.api.member.helper.dto.HelperApplyInfo;
import com.dobda.apiserver.api.member.helper.dto.HelperApplyStateInfo;
import com.dobda.apiserver.api.member.helper.dto.MemberHelperApplyDTO;
import com.dobda.apiserver.api.member.helper.entity.HelperApply;
import com.dobda.apiserver.api.member.helper.entity.MemberHelper;
import com.dobda.apiserver.api.member.user.MemberUserRepository;
import com.dobda.apiserver.api.member.user.NotificationMemberRepository;
import com.dobda.apiserver.api.member.user.entity.MemberUser;
import com.dobda.apiserver.api.member.user.entity.NotificationMember;
import com.dobda.apiserver.api.order.dto.RetryRefundAllDto;
import com.dobda.apiserver.common.enums.HelperApplyState;
import com.dobda.apiserver.common.enums.MemberUserLevel;
import com.dobda.apiserver.common.enums.ResourceType;
import com.dobda.apiserver.config.DobdaProperties;
import com.dobda.apiserver.security.authprovider.PasswordUtil;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.tomcat.util.http.fileupload.FileUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.*;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.io.IOException;
import java.math.BigInteger;
import java.nio.file.Paths;
import java.security.NoSuchAlgorithmException;
import java.time.ZonedDateTime;
import java.util.Optional;
import java.util.stream.Collectors;

@Slf4j
@Service("memberHelperService")
@RequiredArgsConstructor
@Transactional(readOnly = true)
public class MemberHelperService {
	
	private final MemberUserRepository memberUserRepository;
	private final MemberHelperRepository memberHelperRepository;
	private final HelperApplyRepository helperApplyRepository;
    private final NotificationMemberRepository notificationMemberRepository;

	@Autowired private DobdaProperties dobdaProperties;
	
	/*
	 * 헬퍼 신청
	 */
	@Transactional
	public void applyHelper(Long memberId, MemberHelperApplyDTO dto) {
        if(memberHelperRepository.existsById(memberId)) {
            throw new BadRequestException("이미 헬퍼로 등록되어 있습니다.");
        }

        Optional<MemberUser> findMemberuser = memberUserRepository.findById(memberId);
        if(findMemberuser.isEmpty()) {
            throw new NotFoundException("사용자 정보를 찾을 수 없습니다.");
        }
        MemberUser memberUser = findMemberuser.get();

        HelperApply helperApply = helperApplyRepository.findByMemberUser_Id(memberId);
        if(helperApply != null) {
            helperApply.setAddress(dto.getAddress());
            helperApply.setBankName(dto.getBankName());
            helperApply.setFintechNum(dto.getFintechNum());
            helperApply.setState(HelperApplyState.WAITING);
            helperApply.setRegTime(ZonedDateTime.now());
            helperApplyRepository.flush();
        } else {
            helperApply = HelperApply.builder()
                    .memberUser(memberUser)
                    .address(dto.getAddress())
                    .bankName(dto.getBankName())
                    .fintechNum(dto.getFintechNum())
                    .build();
            helperApplyRepository.save(helperApply);
        }

		String resourcesPath = Paths.get(dobdaProperties.getUploadPath(), "admin").toString();
        File idCardUploadDir = new File(resourcesPath, "idCard");
        File eachIdCardUploadFile = new File(idCardUploadDir, memberId.toString());	//user id로 신분증 파일이 저장될 경로 생성

        // 이미 파일이 있는 경우 삭제
        if(eachIdCardUploadFile.exists()) {
            try {
                FileUtils.forceDelete(eachIdCardUploadFile);
            } catch (IOException e) {
                e.printStackTrace();
            }
        } else {
            idCardUploadDir.mkdirs();
        }
		
		MultipartFile multipartFile = dto.getIdCard();
		if(multipartFile==null) {
            throw new BadRequestException("신분증 사진은 필수 제출 사항입니다.");
        }
		
		try {
			if(!multipartFile.getContentType().toLowerCase().startsWith("image/")) {
				throw new BadRequestException("잘못된 형식의 파일입니다.");
			}
			multipartFile.transferTo(eachIdCardUploadFile);
		}catch (Exception e) {
			 try {
                 FileUtils.forceDelete(eachIdCardUploadFile);
             } catch (IOException ignore) { }

             throw new InternalServerErrorException(e);
		}
	}
	
	/*
	 * 헬퍼 지원 상태 조회 (사용자)
	 */
	public HelperApplyStateInfo getHelperApplyState(Long id) {
		Optional<HelperApply> apply =  helperApplyRepository.findById(id);
		if(apply.isEmpty()) {return null;}
		
		return HelperApplyStateInfo.fromEntity(apply.get());
	}

	/*
	 *	승인 전 헬퍼 지원 취소 (사용자) 
	 */
	@Transactional
	public void cancelApply(Long id) {
		Optional<HelperApply> apply =  helperApplyRepository.findById(id);
		
		if(apply.isEmpty()) {throw new NotFoundException("헬퍼 지원 내역이 없습니다.");}
		if(apply.get().getState() != HelperApplyState.WAITING) {
			throw new BadRequestException("헬퍼 지원 취소는 대기 상태일 때만 가능합니다.");
		}
		helperApplyRepository.deleteById(id);
        removeIdCardFile(id);
	}

	/*
     * 헬퍼 지원 회원 목록 모두 조회 (관리자)
     */
    public Page<HelperApplyInfo> findAllHelperApplicants(Integer page){
        int pageNum = 0;
        if(page != null) {
            pageNum = page - 1;
        }
        if(pageNum < 0) {
            pageNum = 0;
        }
        Pageable pageable = PageRequest.of(pageNum, 25, Sort.Direction.ASC, "regTime");
        Page<HelperApply> findResults = helperApplyRepository.findByState(HelperApplyState.WAITING, pageable);

        return new PageImpl(
                findResults.getContent().stream()
                        .map(HelperApplyInfo::fromEntity)
                        .collect(Collectors.toList()),
                pageable, findResults.getTotalElements());
    }
   
    
    /*
     * 헬퍼 지원자 승인 (관리자 담당) 
     */
    @Transactional
    public void approveHelperApply(Long id) {
        if(memberHelperRepository.existsById(id)) {
            throw new BadRequestException("이미 헬퍼로 등록되어 있습니다.");
        }

        Optional<HelperApply> apply = helperApplyRepository.findById(id);
    	if(apply.isEmpty()) {throw new NotFoundException("해당 ID의 헬퍼 지원자가 없습니다.");}
    	
    	//헬퍼 지원 상태 변경 (waiting->completed) 
    	HelperApply helperApply = apply.get();
        if(helperApply.getState() != HelperApplyState.WAITING) {
            throw new BadRequestException("이미 승인되거나 거부된 신청입니다.");
        }
    	helperApply.setState(HelperApplyState.COMPLETE);
    	
    	//member 테이블 회원등급 변경 (USER->HELPER)
    	MemberUser memberUser = helperApply.getMemberUser();
    	memberUser.setLevel(MemberUserLevel.HELPER);
    	
    	//헬퍼 테이블에 저장 (save) memberHelper 
    	MemberHelper helper = MemberHelper.builder()
    							.memberUser(memberUser)
    							.address(helperApply.getAddress())
    							.bankName(helperApply.getBankName())
    							.fintechNum(helperApply.getFintechNum())
    							.build();
    	memberHelperRepository.save(helper);
        helperApplyRepository.delete(helperApply);

        NotificationMember newNoti = NotificationMember.builder()
                .memberUser(helperApply.getMemberUser())
                .message("헬퍼 신청이 승인되었습니다.")
                .targetType(ResourceType.REGISTER_HELPER)
                .targetId(BigInteger.valueOf(id))
                .build();
        notificationMemberRepository.save(newNoti);
    }
    
    
    /*
	 * 헬퍼 지원자 승인 거절 (관리자 담당) 
	 */
    @Transactional
    public void rejectHelperApply(Long id) {
    	Optional<HelperApply> apply = helperApplyRepository.findById(id);
    	if(apply.isEmpty()) {throw new NotFoundException("해당 ID의 헬퍼 지원자가 없습니다.");}
    	
    	HelperApply helperApply = apply.get();
        if(helperApply.getState() != HelperApplyState.WAITING) {
            throw new BadRequestException("이미 승인되거나 거부된 신청입니다.");
        }
        helperApply.setState(HelperApplyState.REJECTED);
        removeIdCardFile(id);

        NotificationMember newNoti = NotificationMember.builder()
                .memberUser(helperApply.getMemberUser())
                .message("헬퍼 신청이 거부되었습니다.")
                .targetType(ResourceType.REGISTER_HELPER)
                .targetId(BigInteger.valueOf(id))
                .build();
        notificationMemberRepository.save(newNoti);
    }
    

    /*
     * 헬퍼 탈퇴 (사용자 권한은 유지) 
     */
    @Transactional
    public void removeHelper(Long id, String pwd) {
        Optional<MemberHelper> findHelper = memberHelperRepository.findById(id);
        if(findHelper.isEmpty()) {
            throw new NotFoundException("헬퍼 사용자 정보를 찾을 수 없습니다.");
        }
        MemberHelper helper = findHelper.get();
        MemberUser user = helper.getMemberUser();

        try {
            byte[] checkDigest = PasswordUtil.genDigest(pwd, user.getRegTime());
            if (!PasswordUtil.compare(checkDigest, user.getPwd())) {
                throw new UnauthorizedException("비밀번호가 일치하지 않습니다.");
            }
        } catch (NoSuchAlgorithmException e) {
            throw new InternalServerErrorException(e);
        }

        user.setMemberHelper(null);
        memberHelperRepository.delete(helper);
        user.setLevel(MemberUserLevel.USER);

        removeIdCardFile(id);
    }

    private void removeIdCardFile(Long id) {
        String resourcesPath = Paths.get(dobdaProperties.getUploadPath(), "admin").toString();
        File idCardUploadDir = new File(resourcesPath, "idCard");
        File eachIdCardUploadFile = new File(idCardUploadDir, id.toString());

        if(eachIdCardUploadFile.exists()) {
            try {
                FileUtils.forceDelete(eachIdCardUploadFile);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    @Transactional
    public void updateBankInfo(MemberHelper helper, RetryRefundAllDto dto) {
        helper.setBankName(dto.getBankName());
        helper.setFintechNum(dto.getFintech());
    }

    @Transactional
    public void updateBankInfo(Long helperId, RetryRefundAllDto dto) {
        Optional<MemberHelper> findHelper = memberHelperRepository.findById(helperId);
        if(findHelper.isEmpty()) {
            throw new NotFoundException("헬퍼 사용자 정보를 찾을 수 없습니다.");
        }
        MemberHelper helper = findHelper.get();

        helper.setBankName(dto.getBankName());
        helper.setFintechNum(dto.getFintech());
    }

    @Transactional
    public void updateAddress(Long helperId, String address) {
        Optional<MemberHelper> findHelper = memberHelperRepository.findById(helperId);
        if(findHelper.isEmpty()) {
            throw new NotFoundException("헬퍼 사용자 정보를 찾을 수 없습니다.");
        }
        MemberHelper helper = findHelper.get();

        helper.setAddress(address);
    }
}//end class
