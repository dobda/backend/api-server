package com.dobda.apiserver.api.member.helper;

import com.dobda.apiserver.api.member.helper.entity.HelperApply;
import com.dobda.apiserver.common.enums.HelperApplyState;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;


public interface HelperApplyRepository extends JpaRepository<HelperApply, Long> {

    public HelperApply findByMemberUser_Id(Long memberId);

    public Page<HelperApply> findByState(HelperApplyState state, Pageable pageable);

	public List<HelperApply> findAllByOrderByRegTimeDesc();		//모두 조회, 등록일 내림차순
	
	public List<HelperApply> findByState(HelperApplyState state);
	
	public List<HelperApply> findByStateOrderByRegTimeDesc(HelperApplyState state);		//상태별 조회, 등록일 내림차순 
}
