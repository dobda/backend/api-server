package com.dobda.apiserver.api.member.helper.dto;

import com.dobda.apiserver.api.member.helper.entity.HelperApply;
import com.dobda.apiserver.common.enums.HelperApplyState;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;

import java.time.ZonedDateTime;

@Data
@Builder
@AllArgsConstructor
public class HelperApplyStateInfo {
	private String address;
	
	private HelperApplyState state;
	
	private ZonedDateTime regTime;
	
	public static HelperApplyStateInfo fromEntity(HelperApply entity) {
		if(entity == null) {return null;}
		
		return builder()
				.address(entity.getAddress())
				.state(entity.getState())
				.regTime(entity.getRegTime())
				.build();
	}
	
}
