package com.dobda.apiserver.api.member.user;

import com.dobda.apiserver.api.member.user.entity.MemberUser;
import org.springframework.data.jpa.repository.JpaRepository;

import java.time.ZonedDateTime;
import java.util.List;
import java.util.Optional;

public interface MemberUserRepository extends JpaRepository<MemberUser, Long> {
		
	public MemberUser findByEmail(String email);
	
	public MemberUser findByNickname(String nickname);

    public MemberUser findByPhoneNum(String phoneNum);

    public List<MemberUser> findByBankTokenExpireTimeBefore(ZonedDateTime time);
}
