package com.dobda.apiserver.api.review.dto;

import lombok.Builder;
import lombok.Data;

import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.math.BigInteger;

@Data
@Builder
public class AddReviewDto {

    @NotNull BigInteger orderId;
	@Size(min = 1, max = 200) private String content;
	@NotNull @Min(0) @Max(5) private Integer evaluation;
}
