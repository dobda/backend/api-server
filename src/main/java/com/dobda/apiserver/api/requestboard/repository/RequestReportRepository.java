package com.dobda.apiserver.api.requestboard.repository;

import com.dobda.apiserver.api.requestboard.entity.RequestReport;
import com.dobda.apiserver.api.requestboard.entity.RequestReportKey;
import org.springframework.data.jpa.repository.JpaRepository;

public interface RequestReportRepository extends JpaRepository<RequestReport, RequestReportKey>{
}
